# Exploring Telematics Data

At CO2OPT, we process millions of datapoints which has is streaming out of the IOT of the transportation industry. 

As a data scientist, you will be navigating through streams from multiple data sources, pre-processing them and creating forecast models out of it. 

You will also be testing your hypothesis across a larger geographic area and hence, you will be dealing with a lots of geo data. 


## Your Task 
In this repository, you shall find ten datasets under the folder: telematics_data 
Each file represents usage data of one vehicle

Your task is to finish the following goals:
1. Find out the number of trips under each of the dataset per week. 
2. Find out the dates where refuelling were made. 
3. Preprocess the dataset to remove outliers and smoothen the fuelLevel datapoint. Demonstrate atleast 3 methods. 
4. Deduce driving behaviour: High/Low Acceleration and Braking. 
5. Find out three principal components on all the data combined. 
6. Optional: Demonstrate any hypothesis of your liking using unsupervised learning technique (For eg., cluster the vehicles based on some set of features).
7. Optional: Visualise the data for the trips per vehicle using Leaflet and Folium.

For your task, you can follow the usage of following libraries: 
1. Pandas
2. Scikit or PyTorch
3. (Optional) Psycopg2 -> you can use this to load the csv data to a postgresql instance and pull data from that back 
4. (Optional) Leaflet and Folium

